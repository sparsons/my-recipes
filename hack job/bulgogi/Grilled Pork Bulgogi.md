# Grilled Pork Bulgogi

Prep Time: 2-24 hours

Serving Size: This is really tough to assume as it all depends on how much extras you add. This will usually feed my family if I make a single batch, so I always make a double as the leftovers are absolutely amazing. For a double batch on the marinade, double everything except the onion. Usually I will do 2/3 an onion instead.

## Ingredients

### Meat & Marinade

- 6 bone in pork chops, roughly half inch thick
- 3/4 cup white sugar
- 1 teaspoon garlic
- 1/2 cup soy sauce
- 1 teaspoon black pepper (fresh ground is best)
- 1 teaspoon sesame oil
- 1 teaspoon ginger (fresh is best, powder is fine)
- 1/2 white onion
- 2 tablespoons of chili paste (totally optional, but gives it a hell of a kick.)

### Extras

- 2 heads of baby bok choy
- 1/2 tin of baby corn
- 1/2 tin of diced water chestnuts
- 1/2 tin of bamboo shoots
- 1 bag of stir fry noodles - [this is what I usually get](https://www.amazon.com/dp/B071RKDHPH?tag=amz-mkt-fox-us-20&ascsubtag=1ba00-01000-a0049-win10-dsk00-nomod-us000-pcomp-feature-scomp-wm-4-wl-sce0&ref=bit_scomp_sav0)

### Optional Garnishes

- Green Onion
- Sesame Seeds

## Instructions

### Prepping the meat

1. Slice the 1/2 white onion into [long strips](https://www.thekitchn.com/how-to-thinly-slice-an-onion-109536)
2. Add all ingredients from marinade into a one gallon ziploc bag and mush around until it's mixed up properly.
3. Marinade for at least 2 hours, but I highly recommend 24-48 hours.

### Prepping the extras

1. Cook the stir fry noodles according to the package instructions and set aside
2. Slice the green top of the bok choy into two separate parts, the green tops and the white bottom. The green top will be used fresh, and the bottom will be cooked. 
3. Drain the cans of baby corn, diced water chestnuts and bamboo shoots, combine half of each can into a bowl with the bottoms of the bok choy

### Cooking the meat

1. Open the bag of meat that's been marinading overnight (hopefully), and take a huge sniff of that delightful stuff. Now go around the house and force everyone else to smell it as well.
2. I separate out the liquid from the meat/onion combo at this stage by getting a strainer, with a bowl under it, and dumping the contents of the bag over the strainer allowing me to catch all that delicious sauce in the bottom pan.
3. Remove the onions from the meat and place them with bowl of prepped extras with the baby corn, diced water chestnuts and bamboo shoots.
4. Fire up your grill to full blast, we want some delicious char on those pork chops. I have a 3 burner propane grill and turn all 3 burners to high. I then let it warm up and test it by dashing some water over the top, if it sizzles off immediately, then it's warm enough for me.
5. Place the pork chops on the grill and let them sit on each side for about 5-6 minutes or until they are cooked through, and there's a bit of char.
6. Take freshly cooked pork chops inside and force everyone to smell them.
7. Slice the pork chops into thin slices, roughly 3mm thick and set them aside. Generally I will put them in a covered bowl.

### Reducing the sauce

1. Now that you have cooked the meat it's time to separate the sauce. Take the sauce and put it into a pan and turn the heat on about medium. You don't want to scorch it as it's got a lot of sugar in there and will ruin it. Allow the sauce to slightly boil until a white gunky crap starts to form on the top.
2. Remove the white gunky stuff from the top by skimming it with a spoon or slotted ladle as best as possible. 
3. After the sauce has thickened a bit, I usually strain the rest of the white gunky crap through a cheese cloth to remove the rest of it. It's just the fat of the pork.

### Cooking the extras

1. Fire up your wok and add whatever oil flavors you like. I generally use peanut oil. 
2. Add in half a cup of the sauce pulled from the bag of meat marinade to this delightful mix
3. Add the bowl of onion, baby corn, diced water chestnuts and bamboo shoots directly to the wok and stir constantly until the onions start to turn translucent.
4. Add in the noodles and cook this mixture until the noodles are slightly browned by the frying. This is a very fast process and can leave you with burnt noodles if you aren't careful. Keep those noodles moving as much as possible and add a little more oil if needed to prevent it from burning. A well seasoned wok should be able to handle this without burning it to bad though.

Now that everything has been cooked you can put it all together in a bowl, top it with whatever garnishes you like and eat it all up! I usually will put down a layer of the cooked veggies, then meat, then the fresh bok choy top, then some green onion and sesame seeds on the top.

### Notes

Although not required I highly recommend marinading the meat for at least 24 hours. If you are going to marinade it long term, stick it in 1 gallon freezer safe bags and put it in the freezer for, honestly, months if you want.
