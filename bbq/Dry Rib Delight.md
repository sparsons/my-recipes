# Dry Rib Delight

## Ingredients

Dry Rub:

- 2 tablespoons smoked paprika
- 2 tablespoons kosher salt
- 2 tablespoons sugar
- 1 chili powder
- 2 teaspoons black pepper
- 1.5 teaspoon garlic
- 1.5 teaspoon onion
- 1.5 teaspoon cumin
- 1.5 teaspoon celery salt
- 1.5 teaspoon cayenne pepper

KC BBQ Sauce (https://www.leaffilter.com/…/top-regional-bbq-sauce-recipe…/):

- 2 tablespoons butter
- 1 small yellow onion, finely chopped (about 1 cup)
- 3 cloves garlic, minced (about 1 tablespoon)
- 2 cups ketchup
- 1/3 cup molasses
- 1/3 cup dark brown sugar
- 1/3 cup apple cider vinegar
- 2 tablespoon yellow mustard
- 1 tablespoon chili powder
- 1 teaspoon freshly ground black pepper
- 1/2 teaspoon cayenne pepper

## Cooking the Ribs

01. Buy baby back ribs from butcher or meat counter
02. Remove membrane from back of ribs: https://www.weber.com/…/removing-the-membrane-from-baby-bac…
03. Generously cover ribs with dry rub on both front and back
04. Wrap ribs in aluminum foil (completely, as sealed as possible), put an X in sharpie on the top of the aluminum foil (meat side)
05. Put ribs in aluminum foil in fridge overnight (I usually do 24 hours before cooking)
06. Pull ribs out of fridge and let sit for 2-3 hours, or until ribs are room temp

Gas Grill

07. Start your gas grill up and crank it all the way up to the highest temp, I usually get it to around 800F
08. Turn temp down to low, put ribs on grill meat side up (with the X showing, from step 4)
09. Let cook for 1 hour, do NOT open grill cover unless your entire house is on fucking fire and you need to save the ribs.
10. Pull the ribs off the grill, and remove the aluminum foil.
11. Turn the grill back to absolute max temp and let the temp come back up
12. Cover the ribs with bbq sauce on the meat side first, then put the ribs meat side up back on the grill and cook for 5-7 minutes, or until the sauce is caramelized and "glued" to the top of the ribs
13. Flip the ribs on the grill and cover the bottom of the ribs with bbq sauce

```THIS IS THE MOST IMPORTANT PART```

14. Cook meat side down until it has the consistency that you want. I usually like the edges a bit black.

## Cooking the Sauce

1. Melt butter in medium saucepan over medium heat. Add onion and cook until softened, about 5 minutes. Add garlic and cook until fragrant, about 30 seconds.
2. Add ketchup, molasses, brown sugar, vinegar, mustard, chili powder, black pepper, and cayenne pepper and stir to combine. Bring to a boil, then reduce heat to low and simmer until slightly thickened, about 30 minutes, stirring frequently.
3. Transfer sauce to the jar of a blender and blend until smooth. Let cool to room temperature, transfer to a jar and store in refrigerator for up to a month.
The great thing about this sauce is that it can be used on almost everything. Slather this over everything from ribs to chicken, beans, or dips. It is so finger licking good, that your guest are sure to love it.
